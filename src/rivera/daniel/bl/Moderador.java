package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Moderador extends Coleccionista{


    private boolean esColeccionista;

    /**
     * Constructor que recibe todos los parámetros de Coleccionista  y los Inicializa En Moderador
     * @param nombre Variable que es utilizada para idenfiticar el nombre del moderador
     * @param username Variable que es utilizada para idenfiticar el username del moderador
     * @param password Variable que es utilizada para idenfiticar el password del moderador
     * @param avatar Variable que es utilizada para idenfiticar el avatar del moderador
     * @param id_coleccionista Variable que es utilizada para idenfiticar el id del moderador
     * @param fecha_nacimiento Variable que es utilizada para idenfiticar la fecha de nacimiento del moderador
     * @param edad  Variable que es utilizada para idenfiticar la edad del moderador
     * @param estado  Variable que es utilizada para idenfiticar el estado del moderador
     * @param correo  Variable que es utilizada para idenfiticar el correo del moderador
     * @param puntuacion  Variable que es utilizada para idenfiticar la puntiacion del moderador
     * @param direccion Variable que es utilizada para idenfiticar la direccion del moderador
     * @param pertenencias Variable que es utilizada para idenfiticar las pertenencias del moderador
     * @param intereses1 Variable que es utilizada para idenfiticar los intereses del moderador
     * @param esColeccionista Variable que es utilizada para idenfiticar si es coleccionista
     */
    public Moderador(String nombre, String username, String password, String avatar, String id_coleccionista, LocalDate fecha_nacimiento, int edad, boolean estado, String correo, double puntuacion, String direccion, ArrayList<Item> pertenencias, ArrayList<Interes> intereses1, boolean esColeccionista) {
        super(nombre, username, password, avatar, id_coleccionista, fecha_nacimiento, edad, estado, correo, puntuacion, direccion, pertenencias, intereses1);
        this.esColeccionista = esColeccionista;
    }


 

    public Moderador(boolean esColeccionista) {
        this.esColeccionista = esColeccionista;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Avatar de la clase Moderador
     * @return la variable de retorno simboliza el avatar
     */
    public boolean isEsColeccionista() {
        return esColeccionista;
    }

    /**
     * metodo utilizado para obtener el valor del booleano privado esColeccionista de la clase Moderador
     * @param esColeccionista Variable que es utilizada para idenfiticar si es coleccionista
     */
    public void setEsColeccionista(boolean esColeccionista) {
        this.esColeccionista = esColeccionista;
    }


     /**
          * Método que compara los Atributos de la Clase Moderador Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Moderador moderador = (Moderador) o;
        return isEsColeccionista() == moderador.isEsColeccionista();
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEsColeccionista());
    }
}

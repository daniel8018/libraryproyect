package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Subasta {
    private LocalDate fecha_creacion;
    private LocalDate fecha_inicio;
    private LocalDate hora_inicio;
    private LocalDate fecha_vencimiento;
    private String subasta_vendedor;
    private double puntuacion_vendedor;
    private String estado_vendedor;
    private double precio_minimo;
    private ArrayList<Item> objetos = new ArrayList<Item>();   //OBJETOS E ITEMS QUE SE VAN A SUBASTAR EN PDF

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_creacion de la clase Subasta
     * @return la variable de retorno simboliza la fecha en la que se crea la subasta
     */
    public LocalDate getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_creacion de la clase Subasta
     * @param fecha_creacion Variable que simboliza la fecha de creacion de la clase Subasta
     *
     */
    public void setFecha_creacion(LocalDate fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_hora_inicio de la clase Subasta
     * @return la variable de retorno simboliza la fecha y hora en la que incia la subasta
     */
    public LocalDate getFecha_inicio() {
        return fecha_inicio;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_inicio de la clase Subasta
     * @param fecha_inicio Variable que simboliza la fecha y hora en la que se inició la subasta
     */
    public void setFecha_inicio(LocalDate fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    /**
     * Metodo utilizado para modificar el atributo privado hora_inicio de la clase Subasta
     * @return El dato de retorno simboliza la hora de inicio de la subasta
     */
    public LocalDate getHora_inicio() {
        return hora_inicio;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada hora_inicio de la clase Subasta
     * @param hora_inicio Variable que simboliza la hora en que se inicia la subasta
     */
    public void setHora_inicio(LocalDate hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_vencimiento de la clase Subasta
     * @return la variable de retorno simboliza la fecha en la que vence la subasta
     */
    public LocalDate getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_vencimiento de la clase Subasta
     * @param fecha_vencimiento Variable que simboliza la fecha de vencimiento de la clase Subasta
     *
     */
    public void setFecha_vencimiento(LocalDate fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada Subasta_vendedor de la clase Subasta
     * @return la variable de retorno simboliza el vendedor asignado a la subasta
     */
    public String getSubasta_vendedor() {
        return subasta_vendedor;
    }

    /**
     * Metodo utilizado para modificar el atributo privado subasta_vendedor de la clase
     * @param subasta_vendedor Variable que simboliza el vendedor asignado a subasta
     *
     */
    public void setSubasta_vendedor(String subasta_vendedor) {
        this.subasta_vendedor = subasta_vendedor;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Puntuacion_vendedor de la clase Subasta
     * @return la variable de retorno simboliza la puntuacion del vendedor de la subasta
     */
    public double getPuntuacion_vendedor() {
        return puntuacion_vendedor;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Puntuacion_vendedor de la clase
     * @param puntuacion_vendedor Variable que simboliza si el vendedor esta conforme del 1 al 5 de la clase Subasta
     *
     */
    public void setPuntuacion_vendedor(double puntuacion_vendedor) {
        this.puntuacion_vendedor = puntuacion_vendedor;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada estado_vendedor de la clase Subasta
     * @return la variable de retorno simboliza si el vendedor está activo o no
     */
    public String getEstado_vendedor() {
        return estado_vendedor;
    }

    /**
     * Metodo utilizado para modificar el atributo privado estado_vendedo de la clase Subasta
     * @param estado_vendedor Variable que simboliza si el vendedor está activo o no de la clase Subasta

     */
    public void setEstado_vendedor(String estado_vendedor) {
        this.estado_vendedor = estado_vendedor;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada precio_minimo de la clase Subasta
     * @return la variable de retorno simboliza el precio minimo asignado
     */
    public double getPrecio_minimo() {
        return precio_minimo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado precio_minimo de la clase subasta
     * @param precio_minimo Variable que simboliza el precio minimo de la subasta de la clase subasta
     */
    public void setPrecio_minimo(double precio_minimo) {
        this.precio_minimo = precio_minimo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada items_subastados de la clase Subasta
     * @return la variable de retorno simboliza los items que van a ser subastados
     */
    public ArrayList<Item> getItems_subastados() {
        return objetos;
    }

    /**
     * Metodo utilizado para modificar el atributo privado items_subastados de la clase Subasta
     * @param items_subastados Variable que simboliza los items subastados en el arraylist
     */
    public void setItems_subastados(ArrayList<Item> items_subastados) {
        this.objetos = items_subastados;
    }

    /**
     * Constructor vacio para la clase Subasta
     */
    public Subasta() {
    }

    /**
     * Constructor que recibe todos los parámetros de Subasta  y los Inicializa
     * @param fecha_creacion Variable que simboliza la fecha de creacion de la subasta de la clase Subasta
     * @param fecha_inicio  Variable que simboliza la hora y fecha de inicio de la clase Subasta
     * @param fecha_vencimiento Variable que simboliza la fecha de vencimiento de la subasta de la clase Subasta
     * @param subasta_vendedor Variable que simboliza el vendedor asignado en la subasta de la clase Subasta
     * @param puntuacion_vendedor Variable que simboliza la puntuacion del vendedor asignado a la subasta de la clase Subasta
     * @param estado_vendedor Variable que simboliza si el vendedor está activo o no
     * @param precio_minimo Variable que simboliza el precio mínimo de la subasta
     * @param objetos Variable que simboliza los items a subastar, provenientes de la clase items
     */
    public Subasta(LocalDate fecha_creacion, LocalDate fecha_inicio, LocalDate hora_inicio, LocalDate fecha_vencimiento, String subasta_vendedor, double puntuacion_vendedor, String estado_vendedor, double precio_minimo, ArrayList<Item> objetos) {
        this.fecha_creacion = fecha_creacion;
        this.fecha_inicio = fecha_inicio;
        this.hora_inicio = hora_inicio;
        this.fecha_vencimiento = fecha_vencimiento;
        this.subasta_vendedor = subasta_vendedor;
        this.puntuacion_vendedor = puntuacion_vendedor;
        this.estado_vendedor = estado_vendedor;
        this.precio_minimo = precio_minimo;
        this.objetos = objetos;
    }

    /**
          * Metodo utilizado para imprimir todos los atributos de la clase Subasta en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Subasta en unico string
          */
    @Override
    public String toString() {
        return "Subasta{" +
                "fecha_creacion=" + fecha_creacion +
                ", fecha_inicio=" + fecha_inicio +
                ", hora_inicio=" + hora_inicio +
                ", fecha_vencimiento=" + fecha_vencimiento +
                ", subasta_vendedor='" + subasta_vendedor + '\'' +
                ", puntuacion_vendedor=" + puntuacion_vendedor +
                ", estado_vendedor='" + estado_vendedor + '\'' +
                ", precio_minimo=" + precio_minimo +
                ", objetos=" + objetos +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Subasta Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subasta subasta = (Subasta) o;
        return Double.compare(subasta.getPuntuacion_vendedor(), getPuntuacion_vendedor()) == 0 &&
                Double.compare(subasta.getPrecio_minimo(), getPrecio_minimo()) == 0 &&
                getFecha_creacion().equals(subasta.getFecha_creacion()) &&
                getFecha_inicio().equals(subasta.getFecha_inicio()) &&
                getHora_inicio().equals(subasta.getHora_inicio()) &&
                getFecha_vencimiento().equals(subasta.getFecha_vencimiento()) &&
                getSubasta_vendedor().equals(subasta.getSubasta_vendedor()) &&
                getEstado_vendedor().equals(subasta.getEstado_vendedor()) &&
                objetos.equals(subasta.objetos);
    }


}

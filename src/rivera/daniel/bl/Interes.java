package rivera.daniel.bl;

import java.util.Objects;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Interes extends Lista {

    private int codigoInteres;
    private String nombre,descripcion;

    /**
     * contructor por defecto de la clase interes
     */
    public Interes(){}

    /**
     * contructor que inicializa los parámetros de la clase intereses
     * @param codigoInteres tipo int, codigo de interes.
     * @param nombre tipo String nombre del interes.
     * @param descripcion tipo String descripcion del interes.
     */

    public Interes(int codigoInteres, String nombre, String descripcion) {
        this.codigoInteres = codigoInteres;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    /**
     *Metodo utilizado para modificar el atributo privado Codigo de Interes de la clase Intereses
     * @return El dato de retorno simboliza el codigo de Interes
     */
    public int getCodigoInteres() {
        return codigoInteres;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada CodigoInteres de la clase Intereses
     * @param codigoInteres: Variable que simboliza el codigo de interes la de la clase  Intereses
     */
    public void setCodigoInteres(int codigoInteres) {
        this.codigoInteres = codigoInteres;
    }
    /**
     *Metodo utilizado para modificar el atributo privado nombre de la clase Intereses
     * @return devuelve el nombre del interes
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Interes
     * @param nombre: dato que almacena el nombre del interes  de manera privada en su clase
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     *Metodo utilizado para modificar el atributo privado descripcion de la clase descripcion
     * @return devuelve la descripcion del interes
     */
    public String getDescripcion() {
        return descripcion;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada descripcion  de la clase Interes
     * @param descripcion: Variable que simboliza la descripcion de la clase Interes
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

     /**
               * Metodo utilizado para imprimir todos los atributos de la clase Interes en un unico String
               * @return la variable de retorno simboliza todos los valores de la clase Interes en unico string
               */
    @Override
    public String toString() {
        return "Interes{" +
                "codigoInteres=" + codigoInteres +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Interes Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interes interes = (Interes) o;
        return getCodigoInteres() == interes.getCodigoInteres() &&
                getNombre().equals(interes.getNombre()) &&
                getDescripcion().equals(interes.getDescripcion());
    }

    /**
     * método sobreEscrito de Lista para ser agregada
     */
    @Override
    public void agregar() {

    }
}

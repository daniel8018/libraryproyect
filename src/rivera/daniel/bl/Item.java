package rivera.daniel.bl;
import java.time.LocalDate;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Item {
    private String nombre;
    private String descripcion;
    private String estado;
    //private icon imagen;
    private LocalDate fecha_compra;
    private LocalDate antiguedad;    //TIEMPO DE ANTIGÜEDAD
    private String categoria;
    private String prueba;

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Item
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo utilizado para asignar el valor de la variable privada nombre de la clase Item
     *
     * @param nombre la variable que simboliza el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Descripcion de la clase Item
     *
     * @return devuelve la descripcion del item
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Descripcion de la clase Item
     *
     * @param descripcion parámetro para guardar la descripcion del item
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada estado de tipo String de la clase Item
     *
     * @return la variable de retorno simboliza el estado del item
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo utilizado para modificar el atributo privado estado de la clase Item
     *
     * @param estado Variable de Estado de tipo Booleano que simboliza el estado del item
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_compra de la clase Item
     *
     * @return la variable de retorno simboliza la fecha en la que se realizó la compra de un item
     */
    public LocalDate getFecha_compra() {
        return fecha_compra;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Fecha_compra de la clase Item
     *
     * @param fecha_compra Variable de  de tipo LocalDate que simboliza la fecha de compra de la clase Item
     */
    public void setFecha_compra(LocalDate fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Antiguedad de la clase Item
     *
     * @return la variable de retorno simboliza que tan antiguo es el item
     */
    public LocalDate getAntiguedad() {
        return antiguedad;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Antiguedad de la clase Item
     *
     * @param antiguedad Variable de antiguedad de tipo LocalDate que simboliza la antiguedad de la clase Item
     */
    public void setAntiguedad(LocalDate antiguedad) {
        this.antiguedad = antiguedad;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Categoria de la clase Item
     *
     * @return la variable de retorno simboliza la categoria del item
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Categoria de la clase item
     *
     * @param categoria Variable de Categoria de tipo String que simboliza que tipo de categoria es la clase Item
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * constructor vacio para la clase item
     */
    public Item() {

    }

    /**
     * Constructor que recibe todos los parámetros de Administrador y los Inicializa
     *
     * @param nombre       Variable de nombre de tipo String que simboliza el nombre del Item
     * @param descripcion  Variable de descripcion de tipo String que simboliza la descripcion del Item
     * @param estado       Variable de estado de tipo String que simboliza el estado del Item
     * @param fecha_compra Variable de fecha_compra de tipo LocalDate que simboliza la fecha en la que se compro el Item
     * @param antiguedad   Variable de antiguedad de tipo LocalDate que simboliza la antiguedad del Item
     * @param categoria    Variable de categoria de tipo String que simboliza la categoria del Item
     */
    public Item(String nombre, String descripcion, String estado, String categoria, LocalDate fecha_compra, LocalDate antiguedad) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.categoria = categoria;
        this.fecha_compra = fecha_compra;
        this.antiguedad = antiguedad;

    }

    public Item(String nombre, String descripcion, String estado, String categoria) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.categoria = categoria;
    }

    public Item(String nombre, String descripcion, String estado, String categoria, String prueba) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.prueba = prueba;
        this.categoria = categoria;
    }

    /**
     * Método utilizado para imprimir todos los atributos de la clase Item en un único String
     *
     * @return la variable de retorno simboliza todos los atributos en un solo String
     */
    public String toString() {
        return "Item{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", estado='" + estado + '\'' +
                ", fecha_compra=" + fecha_compra +
                ", antiguedad=" + antiguedad +
                ", categoria='" + categoria + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase - Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return getNombre().equals(item.getNombre()) &&
                getDescripcion().equals(item.getDescripcion()) &&
                getEstado().equals(item.getEstado()) &&
                getFecha_compra().equals(item.getFecha_compra()) &&
                getAntiguedad().equals(item.getAntiguedad()) &&
                getCategoria().equals(item.getCategoria()) &&
                prueba.equals(item.prueba);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNombre(), getDescripcion(), getEstado(), getFecha_compra(), getAntiguedad(), getCategoria(), prueba);
    }
}

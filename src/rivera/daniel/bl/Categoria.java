package rivera.daniel.bl;

import java.util.Objects;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Categoria extends Lista{
    private String codigo, nombre;

     /**
          * Constructor vacio para la clase Categoría
          */
    public Categoria() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Categoria y los Inicializa
     * @param codigo Variable que es utilizada para identificar el codigo  de la clase Categoria
     * @param nombre Variable que es utilizada para identificar el nombre  de la clase Categoria
     */
    public Categoria(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo privado codigo de la clase Categoria
     * @return El dato de retorno simboliza el codigo de la categoria
     *
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada  codigo de la clase Categoria
     * @param codigo Variable que es utilizada para obtener el codigo  de la clase Categoria
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre de la clase Categoria
     * @return El dato de retorno simboliza el nombre de la categoria
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Categoria
     * @param nombre Variable que es utilizada para simbolizar el nombre de la clase Categoria
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

      /**
       * Metodo utilizado para imprimir todos los atributos de la clase Categoria en un unico String
       * @return la variable de retorno simboliza todos los valores de la clase Categoria en unico string
                */
    @Override
    public String toString() {
        return "Categoria{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Categoria Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return getCodigo().equals(categoria.getCodigo()) &&
                getNombre().equals(categoria.getNombre());
    }

    /**
     * método sobreEscrito de Lista para ser agregada
     */
    @Override
    public void agregar() {

    }
}

package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Coleccionista extends Usuario{


    private String avatar;
    private String id_coleccionista;
    private LocalDate fecha_nacimiento;
    private int edad;
    private boolean estado;
    private String correo;
    private double puntuacion;
    private String direccion;

    private ArrayList<Item> pertenencias = new ArrayList<Item>();
    private ArrayList<Interes> Intereses = new ArrayList<Interes>();



    /**
     * metodo utilizado para obtener el valor de la variable privada Avatar de la clase coleccionista
     * @return la variable de retorno simboliza el nombre
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Avatar de la clase coleccionista
     * @param avatar Variable de Avatar de tipo String que simboliza el avatar de la clase coleccionista
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada id_admin de la clase coleccionista
     * @return la variable de retorno simboliza el id del coleccionista
     */
    public String getId_coleccionista() {
        return id_coleccionista;
    }

    /**
     * Metodo utilizado para modificar el atributo privado id_coleccionista  de la clase Coleccionista
     * @param id_coleccionista Variable de id de tipo int que simboliza el id del coleccionista de la clase Coleccionista
     */
    public void setId_coleccionista(String id_coleccionista) {
        this.id_coleccionista = id_coleccionista;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_nacimiento de la clase coleccionista
     * @return la variable de retorno simboliza la fecha de nacimiento del coleccionista
     */
    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    /**
     * Metodo utilizado para modificar el atributo privado fecha_nacimiento de la clase coleccionista
     * @param fecha_nacimiento Variable de fecha de nacimiento de tipo LocalDate que simboliza el nacimiento de la clase coleccionista
     */
    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada edad de la clase coleccionista
     * @return la variable de retorno simboliza la edad del coleccionista
     */
    public int getEdad() {
        return edad;
    }
    /**
     * Metodo utilizado para modificar el atributo privado Edad de la clase coleccionista
     * @param edad Variable de edad de tipo int que simboliza la edad de la clase coleccionista
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada estado de tipo booleano de la clase coleccionista
     * @return la variable de retorno simboliza si el coleccionista está activo o no.
     */
    public boolean isEstado() {
        return estado;
    }
    /**
     * Metodo utilizado para modificar el atributo privado estado de la clase coleccionista
     * @param estado Variable de Estado de tipo Booleano que simboliza si el coleccionista está activo de la clase coleccionista
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada correo de la clase coleccionista
     * @return la variable de retorno simboliza el correo del coleccionista
     */
    public String getCorreo() {
        return correo;
    }
    /**
     * Metodo utilizado para modificar el atributo privado correo de la clase coleccionista
     * @param correo Variable de correo de tipo String que simboliza el correo de la clase coleccionista
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada puntuacion de la clase coleccionista
     * @return la variable de retorno simboliza la puntuacion
     */
    public double getPuntuacion() {
        return puntuacion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado puntuacion de la clase  coleccionista
     * @param puntuacion
     */
    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

   /**
        * metodo utilizado para obtener el valor de la variable privada direccion de la clase coleccionista
        * @return la variable de retorno simboliza la direccion
        */
    public String getDireccion() {
        return direccion;
    }

 /**
      * Metodo utilizado para modificar el atributo Direccion de la clase coleccionista
      * @param direccion la variable simboliza la Direccion
      */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    /**
     * metodo utilizado para obtener el valor de la variable privada Pertenencias que es un Arraylist de la clase Coleccionista
     * @return
     */
    public ArrayList<Item> getPertenencias() {
        return pertenencias;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Pertenencias que es un Arraylist de la clase Coleccionista
     * @param pertenencias
     */
    public void setPertenencias(ArrayList<Item> pertenencias) {
        this.pertenencias = pertenencias;
    }

    /**
     * Metodo utilizado para modificar el ArrayList privado de la clase Intereses
     * @return El dato de retorno simboliza el ArrayList de intereses
     */
    public ArrayList<Interes> getIntereses() {
        return Intereses;
    }

    /**
     * metodo utilizado para obtener el valor del ArrayList privado Intereses de la clase Interes
     * @param intereses Variable que simboliza el parametro de la clase Intereses
     */
    public void setIntereses(ArrayList<Interes> intereses) {
        Intereses = intereses;
    }

    /**
     * Constructor que recibe todos los parámetros de Coleccionista y los Inicializa
     * @param nombre Variable de nombre de tipo String que simboliza el nombre de la clase Coleccionista
     * @param avatar Variable de avatar de tipo String que simboliza el avatar de la clase Coleccionista
     * @param id_coleccionista Variable de id de tipo String que simboliza el id del coleccionsita de la clase Coleccionista
     * @param fecha_nacimiento Variable de fecha_nacimiento de tipo LocalDate  que simboliza la fecha de nacimiento de la clase Coleccionista
     * @param edad Variable de edad de tipo int que simboliza la edad de la clase Coleccionista
     * @param estado Variable de estado de tipo Boolean que simboliza el estado de la clase Coleccionista
     * @param correo Variable de correo de tipo String que simboliza el correo de la clase Coleccionista
     * @param puntuacion Variable de puntuacion de tipo double que simboliza la puntuacion de la clase Coleccionista
     * @param direccion Variable de direccion de tipo String que simboliza la direccion de la clase Coleccionista
     * @param pertenencias variable de pertenencias de tipo ArrayList que simboliza las Pertencias de la clase Coleccionista
     */
    public Coleccionista(String nombre, String username, String password, String avatar, String id_coleccionista, LocalDate fecha_nacimiento, int edad, boolean estado, String correo, double puntuacion, String direccion, ArrayList<Item> pertenencias, ArrayList<Interes> intereses1) {
        super(nombre, username, password);
        this.avatar = avatar;
        this.id_coleccionista = id_coleccionista;
        this.fecha_nacimiento = fecha_nacimiento;
        this.edad = edad;
        this.estado = estado;
        this.correo = correo;
        this.puntuacion = puntuacion;
        this.direccion = direccion;
        this.pertenencias = pertenencias;
        Intereses = intereses1;
    }

    /**
     * Constructor vacio para la clase Coleccionista que extiende de Usuario
     */
    public Coleccionista(){
     super();
    }

    /**
     * Metodo utilizado para imprimir todos los atributos de la clase Coleccionista en un unico String Incluye los atributos de su padre
     * @return la variable de retorno simboliza todos los valores de la clase Coleccionista en unico string
     */
    @Override
    public String toString() {
        return "Coleccionista{" +
                "avatar='" + avatar + '\'' +
                ", id_coleccionista='" + id_coleccionista + '\'' +
                ", fecha_nacimiento=" + fecha_nacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", correo='" + correo + '\'' +
                ", puntuacion=" + puntuacion +
                ", direccion='" + direccion + '\'' +
                ", pertenencias=" + pertenencias +
                ", Intereses=" + Intereses +
                ", nombre='" + nombre + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Coleccionista Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coleccionista that = (Coleccionista) o;
        return getEdad() == that.getEdad() &&
                isEstado() == that.isEstado() &&
                Double.compare(that.getPuntuacion(), getPuntuacion()) == 0 &&
                getAvatar().equals(that.getAvatar()) &&
                getId_coleccionista().equals(that.getId_coleccionista()) &&
                getFecha_nacimiento().equals(that.getFecha_nacimiento()) &&
                getCorreo().equals(that.getCorreo()) &&
                getDireccion().equals(that.getDireccion()) &&
                getIntereses().equals(that.getIntereses()) &&
                getPertenencias().equals(that.getPertenencias()) &&
                getIntereses().equals(that.getIntereses());
    }

}
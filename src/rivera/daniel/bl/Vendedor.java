package rivera.daniel.bl;

import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Vendedor {

String nombre;
String correo;
String direccion;
int puntuacion;
String estado;
double precio_minimo;

    /**
         * metodo utilizado para obtener el valor de la variable privada nombre de la clase Vendedor
         * @return la variable de retorno simboliza el nombre del vendedor
         */
    public String getNombre() {
        return nombre;
    }
/**
     * metodo utilizado para asignarle valor a la variable privada nombre de la clase Vendedor
     * @param nombre Variable de nombre de tipo String que simboliza el nombre de la clase vendedor
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
/**
     * metodo utilizado para obtener el valor de la variable privada correo de la clase Vendedor
     * @return la variable de retorno simboliza el correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Correo de la clase Vendedor
     * @param correo Variable de correo de tipo String que simboliza el correo de la clase Vendedor
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }
/**
     * metodo utilizado para obtener el valor de la variable privada direccion de la clase Vendedor
     * @return la variable de retorno simboliza la direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Direccion de la clase Vendedor
     * @param direccion Variable de direccion de tipo String que simboliza la direccion de la clase vendedor
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado puntuacion de la clase vendedor
     * @return El dato de retorno simboliza la puntuacion del vendedor
     */
    public int getPuntuacion() {
        return puntuacion;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada puntuacion de la clase Vendedor
     * @param puntuacion Variable que simboliza la puntuacion de la clase vendedor
     */
    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado estado  de la clase vendedor
     * @return El dato de retorno simboliza el estado del vendedor
     */
    public String getEstado() {
        return estado;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada estado de la clase vendedor
     * @param estado Variable que simboliza el estado la de la clase vendedor
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo utilizado para modificar el atributo privado precio minimo aceptado de la clase vendedor
     * @return El dato de retorno simboliza el precio minimo
     */
    public double getPrecio_minimo() {
        return precio_minimo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada precio de la clase vendedor
     * @param precio_minimo Variable que simboliza el precio minimo de la clase vendedor
     */
    public void setPrecio_minimo(double precio_minimo) {
        this.precio_minimo = precio_minimo;
    }

    /**
     * Constructor vacio para la clase Vendedor
     */
    public Vendedor() {
    }

    /**
     * Constructor que recibe todos los parámetros de Vendedor y los Inicializa
     * @param nombre Variable de nombre de tipo String que simboliza el nombre de la clase Vendedor
     * @param correo Variable de correo de tipo String que simboliza el correo de la clase Vendedor
     * @param direccion Variable de direccion de tipo String que simboliza la direccion de la clase Vendedor
     */
    public Vendedor(String nombre, String correo, String direccion, int puntuacion, String estado, double precio_minimo) {
        this.nombre = nombre;
        this.correo = correo;
        this.direccion = direccion;
        this.puntuacion = puntuacion;
        this.estado = estado;
        this.precio_minimo = precio_minimo;
    }

    /**
     * Método utilizado para imprimir todos los atributos de la clase vendedor en un unico String
     * @return la variable de retorno simboliza todos los valores de la clase vendedor en unico string
     */
    @Override
    public String toString() {
        return "Vendedor{" +
                "nombre='" + nombre + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", puntuacion=" + puntuacion +
                ", estado='" + estado + '\'' +
                ", precio_minimo=" + precio_minimo +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Vendedor Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendedor vendedor = (Vendedor) o;
        return getPuntuacion() == vendedor.getPuntuacion() &&
                Double.compare(vendedor.getPrecio_minimo(), getPrecio_minimo()) == 0 &&
                getNombre().equals(vendedor.getNombre()) &&
                getCorreo().equals(vendedor.getCorreo()) &&
                getDireccion().equals(vendedor.getDireccion()) &&
                getEstado().equals(vendedor.getEstado());
    }

}


package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Oferta {
    private String nombre;
    private int puntuacion_oferente;
    private double precio;
    private LocalDate fecha_oferta;
    private ArrayList<Coleccionista>Coleccionista_oferta;

/**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Oferta
     * @return la variable de retorno simboliza el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo nombre  de la clase Oferta
     * @param nombre la variable simboliza el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Puntuacion_oferente de la clase Oferta
     * @return la variable de retorno simboliza la puntiacion que obtuvo el oferente
     */
    public int getPuntuacion_oferente() {
        return puntuacion_oferente;
    }

    /**
     * Metodo utilizado para modificar el atributo privado puntuacion_oferente de la clase Oferta
     * @param puntuacion_oferente Variable de puntiacion_oferente de tipo int que simboliza la puntuacion del oferente de la clase Oferta
     */
    public void setPuntuacion_oferente(int puntuacion_oferente) {
        this.puntuacion_oferente = puntuacion_oferente;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada precio de la clase Oferta
     * @return la variable de retorno simboliza el precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Precio de la clase Oferta
     * @param precio Variable de precio de tipo double que simboliza el precio del item de la clase Oferta
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_oferta de la clase Oferta
     * @return la variable de retorno simboliza la fecha en la que se hizo la oferta
     */
    public LocalDate getFecha_oferta() {
        return fecha_oferta;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_oferta  de la clase Oferta
     * @param fecha_oferta Variable de  de tipo LocalDate que simboliza la fecha en la que se hizo la oferta de la clase Oferta
     */
    public void setFecha_oferta(LocalDate fecha_oferta) {
        this.fecha_oferta = fecha_oferta;
    }

    /**
     * Constructor que recibe todos los parámetros de Oferta y los Inicializa
     */
    public Oferta() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Oferta  y los Inicializa
     * @param nombre Variable de nombre de tipo String que simboliza el nombre de la clase Oferta
     * @param puntuacion_oferente Variable de puntuacion_oferente de tipo int que simboliza el la puntuacion de la clase Oferta
     * @param precio Variable de precio de tipo Double que simboliza el precio de la clase Oferta
     * @param fecha_oferta Variable de fecha_oferta de tipo LocalDate que simboliza la fecha de la oferta de la clase Oferta
     */
    public Oferta(String nombre, int puntuacion_oferente, double precio, LocalDate fecha_oferta) {
        this.nombre = nombre;
        this.puntuacion_oferente = puntuacion_oferente;
        this.precio = precio;
        this.fecha_oferta = fecha_oferta;
    }

    /**
          * Metodo utilizado para imprimir todos los atributos de la clase Oferta en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Oferta en unico string
          */
    public String toString() {
        return "Oferta{" +
                "nombre='" + nombre + '\'' +
                ", puntuacion_oferente=" + puntuacion_oferente +
                ", precio=" + precio +
                ", fecha_oferta=" + fecha_oferta +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Oferta Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oferta oferta = (Oferta) o;
        return getPuntuacion_oferente() == oferta.getPuntuacion_oferente() &&
                Double.compare(oferta.getPrecio(), getPrecio()) == 0 &&
                getNombre().equals(oferta.getNombre()) &&
                getFecha_oferta().equals(oferta.getFecha_oferta()) &&
                Coleccionista_oferta.equals(oferta.Coleccionista_oferta);
    }


}

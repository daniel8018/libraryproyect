package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Administrador extends Usuario{

    private String avatar;
    private int id_admin;
    private LocalDate fecha_nacimiento;
    private int edad;
    private boolean estado;
    private String correo;


    /**
     * metodo utilizado para obtener el valor de la variable privada Avatar de la clase Administrador
     * @return la variable de retorno simboliza el nombre
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Avatar de la clase Administrador
     * @param avatar Variable de Avatar de tipo String que simboliza el avatar de la clase Administrador
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada id_admin de la clase Administrador
     * @return la variable de retorno simboliza el id del administrador
     */
    public int getId_admin() {
        return id_admin;
    }


    /**
     * Metodo utilizado para modificar el atributo privado id_admin de la clase Administrador
     * @param id_admin Variable de id de tipo int que simboliza el Id de la clase administrador
     */
    public void setId_admin(int id_admin) {
        this.id_admin = id_admin;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_nacimiento de la clase Administrador
     * @return la variable de retorno simboliza la fecha de nacimiento del administrador
     */
    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_nacimiento de la clase Administrador
     * @param fecha_nacimiento Variable de fecha de nacimiento de tipo LocalDate que simboliza el nacimiento de la clase Administrador
     */
    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada edad de la clase administrador
     * @return la variable de retorno simboliza la edad del administrador
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Edad de la clase Administrador
     * @param edad Variable de edad de tipo int que simboliza la edad de la clase Administrador
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada estado de tipo booleano de la clase Administrador
     * @return la variable de retorno simboliza si el administrador está activo o no.
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * Metodo utilizado para modificar el atributo privado estado de la clase Administrador
     * @param estado Variable de Estado de tipo Booleano que simboliza si el administrador está activo de la clase Administrador
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada correo de la clase Administrador
     * @return la variable de retorno simboliza el correo del administrador
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado correo de la clase Administrador
     * @param correo Variable de correo de tipo String que simboliza el correo de la clase Administrador
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }


    /**
     * Constructor que recibe todos los parámetros de Administrador y los Inicializa.
     * @param nombre Variable  de Administrador de tipo String que simboliza el nombre del administrador
     * @param avatar Variable de Administrador de tipo String que simboliza el avatar del Administrador
     * @param id_admin Variable  de Administrador de tipo int que simboliza el ID del Administrador
     * @param fecha_nacimiento Variable  de fecha de nacimiento Administrador de tipo LocalDate que simboliza el nacimiento del admin
     * @param edad Variable de Edad de Administrador de tipo int que simboliza
     * @param estado Variable  de estado de tipo booleano  que simboliza si el administrador está activo o no
     * @param correo Variable  de administrador de tipo  String que simboliza el correo del administrador
     * @param password Variable  de Administrador de tipo String que simboliza el password del administrador
     */
    public Administrador(String nombre, String username, String password, String avatar, int id_admin, LocalDate fecha_nacimiento, int edad, boolean estado, String correo) {
        super(nombre, username, password);
        this.avatar = avatar;
        this.id_admin = id_admin;
        this.fecha_nacimiento = fecha_nacimiento;
        this.edad = edad;
        this.estado = estado;
        this.correo = correo;
    }

    /**
     * Constructor por defecto (vacio) de la clase administrador extendida de Usuario.
     */
    public Administrador() {
      super();
    }

    /**
     * Método utilizado para imprimir todos los atributos de la clase Administrador en un único String e incluye lo de su padre
     * @return la variable de retorno simboliza el String que contiene los atributos
     */
    @Override
    public String toString() {
        return "Administrador{" +
                "avatar='" + avatar + '\'' +
                ", id_admin=" + id_admin +
                ", fecha_nacimiento=" + fecha_nacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", correo='" + correo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    /**
     * Método que compara los Atributos de la Clase Administrador Para saber si los mismos se repiten
     * @param o es el objeto a comparar, para saber si es igual
     * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Administrador that = (Administrador) o;
        return getId_admin() == that.getId_admin() &&
                getEdad() == that.getEdad() &&
                isEstado() == that.isEstado() &&
                getAvatar().equals(that.getAvatar()) &&
                getFecha_nacimiento().equals(that.getFecha_nacimiento()) &&
                getCorreo().equals(that.getCorreo());
    }

}

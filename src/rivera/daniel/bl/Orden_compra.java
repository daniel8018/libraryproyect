package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.Objects;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Orden_compra {
    private String nombre_ganador;
    private LocalDate fecha_orden;
    private String detalle_items;
    private double precio_total;
    private int calificacion_vendedor;
    private int calificacion_ganador;
    private boolean entrega;

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre_ganador de la clase orden_compra
     * @return la variable de retorno simboliza el nombre del ganador
     */
    public String getNombre_ganador() {
        return nombre_ganador;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre_ganador de la clase orden_compra
     * @param nombre_ganador Variable de nombre_ganador de tipo String  que simboliza El nombre del ganador
     */
    public void setNombre_ganador(String nombre_ganador) {
        this.nombre_ganador = nombre_ganador;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha_orden de la clase orden_compra
     * @return la variable de retorno simboliza la fecha en la que se hizo la orden de compra
     */
    public LocalDate getFecha_orden() {
        return fecha_orden;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha_orden  de la clase orden_compra
     * @param fecha_orden Variable de fecha_orden de tipo LocalDate que simboliza la fecha en la que se hizo la compra
     */
    public void setFecha_orden(LocalDate fecha_orden) {
        this.fecha_orden = fecha_orden;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada detalle_items de la clase orden_compra
     * @return la variable de retorno simboliza el detalle de los items
     */
    public String getDetalle_items() {
        return detalle_items;
    }

    /**
     * Metodo utilizado para modificar el atributo privado de la clase orden_compra
     * @param detalle_items Variable de detalle_items de tipo String que simboliza El detalle de los items de la clase orden_compra
     */
    public void setDetalle_items(String detalle_items) {
        this.detalle_items = detalle_items;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada precio_total de la clase orden_compra
     * @return la variable de retorno simboliza el precio total de la orden
     */
    public double getPrecio_total() {
        return precio_total;
    }

    /**
     * Metodo utilizado para modificar el atributo privado precio_total de la clase orden_compra
     * @param precio_total Variable de precio_total de tipo double que simboliza el precio total de la clase orden_compra
     */
    public void setPrecio_total(double precio_total) {
        this.precio_total = precio_total;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada calificacion_vendedor de la clase Orden_compra
     * @return la variable de retorno simboliza la calificacion que dio el vendedor
     */
    public int getCalificacion_vendedor() {
        return calificacion_vendedor;
    }

    /**
     * Metodo utilizado para modificar el atributo privado califacion_vendedor de la clase orden_compra
     * @param calificacion_vendedor Variable de calificacion_vendedor  de tipo int  de la clase  orden_compra
     */
    public void setCalificacion_vendedor(int calificacion_vendedor) {
        this.calificacion_vendedor = calificacion_vendedor;
    }
    /**
     * metodo utilizado para obtener el valor de la variable privada calificacion_ganador de la clase Orden_compra
     * @return la variable de retorno simboliza la calificacion que dio el ganador
     */
    public int getCalificacion_ganador() {
        return calificacion_ganador;
    }
    /**
     * Metodo utilizado para modificar el atributo privado califacion_ganador de la clase orden_compra
     * @param calificacion_ganador Variable de calificacion_ganador  de tipo int  de la clase  orden_compra
     */
    public void setCalificacion_ganador(int calificacion_ganador) {
        this.calificacion_ganador = calificacion_ganador;
    }

    /**
     * Metodo utilizado para modificar el atributo privado entrega de la clase orden_compra
     * @return la variable de retorno simboliza si el item fue entregado al ganador o no
     */
    public boolean isEntrega() {
        return entrega;
    }

    /**
     * Metodo utilizado para modificar el atributo privado entrega de la clase orden_compra
     * @param entrega Variable de entrega de tipo boolean que simboliza si se realizó la entrega de la clase orden_compra
     */
    public void setEntrega(boolean entrega) {
        this.entrega = entrega;
    }

    /**
     * Constructor vacio para la clase orden_compra
     */
    public Orden_compra() {
    }

    /**
     * Constructor que recibe todos los parámetros de  orden_compra  y los Inicializa
     * @param nombre_ganador Variable que simboliza el ganador de la clase orden_compra
     * @param fecha_orden Variable que simboliza la fecha de la orden de la clase orden_compra
     * @param detalle_items Variable que simboliza el detalle de los items de la clase orden_compra
     * @param precio_total Variable que simboliza el precio total de la orden de la clase orden_compra
     * @param calificacion_vendedor Variable que simboliza la calificacion del vendedor de la clase orden_compra
     * @param calificacion_ganador Variable que simboliza la calificacion del ganador de la clase orden_compra
     * @param entrega Variable que simboliza si se hizo la entrega del item de la clase orden_compra
     */
    public Orden_compra(String nombre_ganador, LocalDate fecha_orden, String detalle_items, double precio_total, int calificacion_vendedor, int calificacion_ganador, boolean entrega) {
        this.nombre_ganador = nombre_ganador;
        this.fecha_orden = fecha_orden;
        this.detalle_items = detalle_items;
        this.precio_total = precio_total;
        this.calificacion_vendedor = calificacion_vendedor;
        this.calificacion_ganador = calificacion_ganador;
        this.entrega = entrega;
    }


    /**
         * Metodo utilizado para imprimir todos los atributos de la clase orden_compra en un unico String
         * @return la variable de retorno simboliza todos los valores de la clase orden_compra en unico string
         */
    public String toString() {
        return "orden_compra{" +
                "nombre_ganador='" + nombre_ganador + '\'' +
                ", fecha_orden=" + fecha_orden +
                ", detalle_items='" + detalle_items + '\'' +
                ", precio_total=" + precio_total +
                ", calificacion_vendedor=" + calificacion_vendedor +
                ", calificacion_ganador=" + calificacion_ganador +
                ", entrega=" + entrega +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Orden de compra Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orden_compra that = (Orden_compra) o;
        return Double.compare(that.getPrecio_total(), getPrecio_total()) == 0 &&
                getCalificacion_vendedor() == that.getCalificacion_vendedor() &&
                getCalificacion_ganador() == that.getCalificacion_ganador() &&
                isEntrega() == that.isEntrega() &&
                getNombre_ganador().equals(that.getNombre_ganador()) &&
                getFecha_orden().equals(that.getFecha_orden()) &&
                getDetalle_items().equals(that.getDetalle_items());
    }

}

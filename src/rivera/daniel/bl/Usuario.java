package rivera.daniel.bl;

import java.util.Objects;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Usuario {
    protected String nombre, username, password;

     /**
          * Constructor vacio para la clase Padre Usuario
          */
    public Usuario() {
    }

    /**
     * Constructor que recibe todos los parámetros de Usuario  y los Inicializa
     * @param nombre Variable que es utilizada para idenfiticar el nombre  de la clase Usuario
     * @param username Variable que es utilizada para idenfiticar el username del Usuario
     * @param password Variable que es utilizada para idenfiticar el password de la clase Usuario
     */
    public Usuario(String nombre, String username, String password) {
        this.nombre = nombre;
        this.username = username;
        this.password = password;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Nombre de la clase Usuario
     * @return El dato de retorno simboliza el nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Usuario
     * @param nombre Variable que es utilizada para idenfiticar el nombre del Usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Username del usuario
     * @return El dato de retorno simboliza el username del usuario
     */
    public String getUsername() {
        return username;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Username del Usuario
     * @param username Variable que es utilizada para idenfiticar el username del Usuario
     *
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Metodo utilizado para modificar el atributo privado password  de la clase Usuario
     * @return El dato de retorno simboliza el password del usuario
     */
    public String getPassword() {
        return password;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada password del usuario
     * @param password Variable que es utilizada para idenfiticar el password del usuario
     */
    public void setPassword(String password) {
        this.password = password;
    }

      /**
      * Metodo utilizado para imprimir todos los atributos de la clase Usuario en un unico String
     * @return la variable de retorno simboliza todos los valores de la clase Usuario en unico string
                */
    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase Usuario Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return getNombre().equals(usuario.getNombre()) &&
                getUsername().equals(usuario.getUsername()) &&
                getPassword().equals(usuario.getPassword());
    }

}
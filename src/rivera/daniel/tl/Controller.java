package rivera.daniel.tl;

import rivera.daniel.bl.Categoria;
import rivera.daniel.bl.Interes;
import rivera.daniel.bl.Item;
import rivera.daniel.dl.Capa;

import java.time.LocalDate;

public class Controller {

    private Capa logica;

    public Controller(){
        logica = new Capa();
    }
            //*******************************************************************

        public void registrarItem(String nombre, String descripcion, String estado,
                                        String categoria, LocalDate fecha, LocalDate antiguedad){
            Item e = new Item(nombre,descripcion,estado,categoria,fecha,antiguedad);
            logica.registrarItem(e);

        }
        public void registrarInteres(int codigoInteres, String nombre, String descripcion){
            Interes i = new Interes(codigoInteres,nombre,descripcion);
            logica.registrarInteres(i);
        }

        public void registrarCategoria(String codigo, String nombre){
        Categoria C = new Categoria(codigo, nombre);
        logica.registrarCategoria(C);
    }

        //*******************************************************************



        public String[] getCategorias(){
            return logica.listarCategorias();
        }
        public String[] getIntereses(){
            return logica.listarIntereses();
        }
        public String[] getItems(){
            return logica.listarItems();
        }
    }


